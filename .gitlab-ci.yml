default:
  image: condaforge/linux-anvil-cos7-x86_64:latest

stages:
  - lint
  - build
  - test
  - deploy

# === Variables ===

variables:
  PACKAGE_VERSION: "2.1.1"

.py-previous: &py-previous
  PYTHON_VERSION: "3.9"
  NUMPY_VERSION: "1.19"

.py-current: &py-current
  PYTHON_VERSION: "3.10"
  NUMPY_VERSION: "1.19"

# === Configurations ===

.configure: &configure
  before_script:
    - |
      cat <<EOF > ~/.condarc
      channel_priority: strict
      channels:
        - conda-forge
        - kimlab
        - ostrokach-forge
        - defaults
      EOF
    - source /opt/conda/etc/profile.d/conda.sh

# === Lint ===

lint:
  stage: lint
  <<: [*configure]
  variables:
    <<: [*py-previous]
  script:
    - cd $(dirname ${CI_CONFIG_PATH})
    - conda create -n lint -y -q "python=${PYTHON_VERSION}" isort flake8 mypy
    - conda activate lint
    - python -m isort ${CI_PROJECT_NAME} --check --diff || true
    - python -m flake8
    - python -m black --config pyproject.toml --check . || true
    - python -m mypy -p ${CI_PROJECT_NAME} || true

# === Build ===

.build: &build
  stage: build
  script:
    - conda install -yq conda conda-build conda-verify conda-forge-pinning
    - cd "${CI_PROJECT_DIR}/.ci/conda"
    - >
      conda build .
      --variant-config-files /opt/conda/conda_build_config.yaml
      --variants "{python: [${PYTHON_VERSION}], numpy: [${NUMPY_VERSION}], python_impl: [cpython]}"
      --no-test
      --output-folder "$CI_PROJECT_DIR/conda-bld"
  artifacts:
    paths:
      - conda-bld

build-py-previous:
  <<: [*configure, *build]
  variables:
    <<: [*py-previous]

build-py-current:
  <<: [*configure, *build]
  variables:
    <<: [*py-current]

# === Test ===

.test:
  stage: test
  extends:
    - .configure
  script:
    # Create conda environment for testing
    - conda create -n test -y -q -c "file://${CI_PROJECT_DIR}/conda-bld"
      "python=${PYTHON_VERSION}" ${CI_PROJECT_NAME} 'pytest>=6.0' pytest-cov hh-suite
    - conda activate test
    # Run tests
    - PKG_INSTALL_DIR=$(python -c "import kmbio; print(kmbio.__path__[0])")
    - cd tests
    - python -m pytest
      -c ../setup.cfg
      --cov="${PKG_INSTALL_DIR}"
      --cov-config=../setup.cfg
      --color=yes
      --import-mode=importlib
      "."
    # Coverage
    - mkdir coverage
    - mv .coverage coverage/.coverage.all
  dependencies:
    - build
  artifacts:
    paths:
      - coverage

test-py-previous:
  extends:
    - .test
  dependencies:
    - build-py-previous
  variables:
    <<: [*py-previous]

test-py-current:
  extends:
    - .test
  dependencies:
    - build-py-current
  variables:
    <<: [*py-current]

# === Docs ===

docs:
  stage: test
  <<: [*configure]
  script:
    # Install required packages
    - conda create -n test -y -q -c "file://${CI_PROJECT_DIR}/conda-bld"
      "python=${PYTHON_VERSION}" ${CI_PROJECT_NAME} nbconvert ipython ipykernel pandoc coverage
    - conda activate test
    - pip install -q sphinx sphinx_rtd_theme recommonmark nbsphinx
    # Build docs
    - sphinx-build ${CI_PROJECT_DIR}/docs public/
    # Coverage
    # - coverage combine coverage/
    # - coverage report
    # - coverage html
    # - mv htmlcov public/
  # coverage: /^TOTAL.* (\d+\%)/
  dependencies:
    - build-py-current
  variables:
    <<: [*py-current]
  artifacts:
    paths:
      - public
  except:
    - triggers

# === Deploy ===

.deploy: &deploy
  stage: deploy
  before_script:
    - conda install twine -yq --no-channel-priority
  script:
    # Rename wheels from `*-linux_x86_64.whl` to `*-manylinux1_x86_64.whl`
    # so that they can be uploaded to PyPI.
    - for i in $CI_PROJECT_DIR/conda-bld/linux-64/*.whl ; do
      echo $i ;
      if [[ $i = *"-linux_x86_64.whl" ]]; then
        mv "${i}" "${i%%-linux_x86_64.whl}-manylinux1_x86_64.whl" ;
      fi ;
      done
    # Development releases go to the Anaconda dev channel
    - if [[ ${PACKAGE_VERSION} = *"dev"* ]] ; then
        anaconda -t $ANACONDA_TOKEN upload $CI_PROJECT_DIR/conda-bld/linux-64/*.tar.bz2 -u kimlab --label dev --force --no-progress ;
       fi
    # Tagged releases go to the Anaconda and PyPI main channels
    - if [[ -n ${CI_COMMIT_TAG} ]] ; then
        anaconda -t $ANACONDA_TOKEN upload $CI_PROJECT_DIR/conda-bld/linux-64/*.tar.bz2 -u kimlab --no-progress ;
        twine upload $CI_PROJECT_DIR/conda-bld/linux-64/*.whl || true ;
      fi

deploy-py-previous:
  <<: *deploy
  dependencies:
    - build-py-previous

deploy-py-current:
  <<: *deploy
  dependencies:
    - build-py-current

# === Pages ===

pages:
  stage: deploy
  before_script:
    - sudo yum install -y -q unzip
  script:
    # Create docs folder for the current version
    - mv public "v${PACKAGE_VERSION%.dev}"
    - mkdir public
    - mv "v${PACKAGE_VERSION%.dev}" public/
    # Create docs folder for each tag
    - 'for tag in $(git tag) ; do
      echo ${tag} ;
      curl -L --header "JOB-TOKEN: $CI_JOB_TOKEN"
      "https://gitlab.com/${CI_PROJECT_NAME}/${CI_PROJECT_NAMESPACE}/-/jobs/artifacts/${tag}/download?job=docs"
      -o artifact-${tag}.zip || continue ;
      unzip artifact-${tag}.zip -d public || continue ;
      rm -rf public/${tag} ;
      mv public/public public/${tag} ;
      done'
    # Create index file
    # TODO:
  dependencies:
    - docs
  artifacts:
    paths:
      - public
  only:
    - master
    - tags
  except:
    - triggers
