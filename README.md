# kmbio

[![conda](https://img.shields.io/conda/dn/kimlab/kmbio.svg)](https://anaconda.org/kimlab/kmbio/)
[![docs](https://img.shields.io/badge/docs-v2.1.1-blue.svg)](https://kimlab.gitlab.io/kmbio/v2.1.1/)
[![pipeline status](https://gitlab.com/kimlab/kmbio/badges/v2.1.1/pipeline.svg)](https://gitlab.com/kimlab/kmbio/commits/v2.1.1/)
[![coverage report](https://gitlab.com/kimlab/kmbio/badges/v2.1.1/coverage.svg)](https://kimlab.gitlab.io/kmbio/v2.1.1/htmlcov/)

Fork of `biopython.PDB` with special powers.

## Development

```bash
python setup.py build_ext --inplace
pip install -e .
```
